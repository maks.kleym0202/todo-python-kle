FROM python:3.10
COPY ./requirements.txt /todo_fastapi_lr1/requirements.txt
ARG PROXY
RUN if [ -z "$PROXY" ]; then \
        pip install --no-cache-dir --upgrade -r /todo_fastapi_lr1/requirements.txt; \
    else \
        pip install --proxy "$PROXY" --no-cache-dir --upgrade -r /todo_fastapi_lr1/requirements.txt; \
    fi
COPY app/ /todo_fastapi_lr1/app
COPY scripts/script.py /todo_fastapi_lr1/script.py
COPY data_for_tests /todo_fastapi_lr1/data_for_tests
COPY ./certs ./certs
WORKDIR /todo_fastapi_lr1/app
CMD [ "uvicorn", "main:app", "--reload","--host", "0.0.0.0", "--port", "80"]
