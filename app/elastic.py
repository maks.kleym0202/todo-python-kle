"""Elasticsearch for Todo
"""

from elasticsearch import Elasticsearch

client = Elasticsearch(
    "https://es01:9200",
    ca_certs="/certs/ca/ca.crt",
    basic_auth=("elastic", "changeme"),
    verify_certs=True
)

analysis_settings = {
    "analysis": {
        "filter": {  # токен фильтр для стоп-слов русского языка и наших трех слов
            "russian_stop": {
                "type": "stop",
                "stopwords": "_russian_"
            },
            "custom_stopwords": {
                "type": "stop",
                "stopwords": ["Игорь", "Валера", "Маша"]
            },
            "russian_stem": {
                "type": "stemmer",
                "language": "russian"
            }
            # "secret_filter": {
            #     "type": "mapping",
            #     "mappings": [
            #         "особой важности => не интересно",
            #         "секретно => не интерессно",
            #         "совершенно секретно => не интересссно"
            #     ]
            # }
        },
        "analyzer": {  # кастомный анализатор
            "my_custom": {
                "type": "custom",
                "tokenizer": "standard",
                "filter": ["lowercase", "russian_stop", "custom_stopwords", "russian_stem"]
            }
        }
    }
}

mapping = {
    "properties": {  # маппинг для индекса настроенный на русский язык
        "id": {"type": "integer"},
        "title": {"type": "text", "analyzer": "my_custom"},
        "description": {"type": "text", "analyzer": "my_custom"},
        "completed": {"type": "boolean"},
        "tag": {"type": "text", "analyzer": "my_custom"},
        "date_of_create": {"type": "date"},
        "date_of_complete": {"type": "text"},
        "belong_to": {"type": "text", "analyzer": "my_custom"},
        "path_to_image": {"type": "text"},
        "hash_image": {"type": "text"},
        "text_file": {"type": "text", "analyzer": "my_custom"}
    }
}

index_name = "2020-3-05-kle"


def create():
    """
    Функция создающая индекс с заданными параметрами
    :return: index_name
    """
    try:
        if not client.indices.exists(index=index_name):
            if client.indices.create(index=index_name, settings=analysis_settings, mappings=mapping):
                return index_name
    except IndexError as err:
        print(f"{err}: Не удалось создать индекс")
        raise err


def add_todo_to_elastic(id, title, description, completed, tag, date_of_create, date_of_complete, belong_to, path_to_image, hash_image):
    """
    Функция добаволения todo в elastic
    :return: document
    """
    try:
        document = {
            "id": id,
            "title": title,
            "description": description,
            "completed": completed,
            "tag": tag,
            "date_of_create": date_of_create,
            "date_of_complete": date_of_complete,
            "belong_to": belong_to,
            "path_to_image": path_to_image,
            "hash_image": hash_image,
            "text_file": "None"
        }

        client.index(index=index_name, document=document)
        print("Запись успешно добавлена")
        return document
    except IndexError as err:
        print(f"{err}: не удалось добавить запись")
        raise err


def search_todo(word):
    """
    Функция поиска todo в elastic
    :return: document
    """
    query = {
        "bool": {
            "should": [
                {
                    "match": {
                        "description": word
                    }
                },
                {
                    "match": {
                        "text_file": word
                    }
                },
                {
                    "match": {
                        "title": word
                    }
                }
            ]
        }
    }

    todos_ids = []

    try:
        result = client.search(index=index_name, query=query)
        for hit in result["hits"]["hits"]:
            todos_ids.append(hit["_source"]["id"])
        return todos_ids
    except Exception as err:
        print(f"{err}: не удалось Todo со словом {word}")
        raise err


def search_todo_tag(tag):
    """
    Функция поиска todo в elastic по тэгу
    :return: document
    """
    query = {
        "match": {
            "tag": tag
        }
    }

    todos_ids = []

    try:
        result = client.search(index=index_name, query=query)
        for hit in result["hits"]["hits"]:
            todos_ids.append(hit["_source"]["id"])
        return todos_ids
    except Exception as err:
        print(f"{err}: не удалось найти Todo с тэгом {tag}")
        raise err


def edit_todo_in_elastic(id, description, tag, completed):
    """
    Функция изменения todo в elastic
    :return: result
    """
    query = {
        "match": {
            "id": id
        }
    }

    try:
        result = client.search(index=index_name, query=query)
        todo_id_in_elastic = result["hits"]["hits"][0]["_id"]
    except Exception as err:
        print(f"{err}: не удалось найти Todo с id {id}")
        raise err

    body = {
        "doc": {
            "description": description,
            "tag": tag,
            "completed": completed,
        }
    }

    try:
        result = client.update(index=index_name, id=todo_id_in_elastic, body=body)
        return result
    except Exception as err:
        print(f"{err}: не удалось поменять Todo")
        raise err


def edit_complete_todo_in_elastic(id, completed):
    """
    Функция изменения состояния выполнения todo в elastic
    :return: result
    """
    query = {
        "match": {
            "id": id
        }
    }

    try:
        result = client.search(index=index_name, query=query)
        todo_id_in_elastic = result["hits"]["hits"][0]["_id"]
    except Exception as err:
        print(f"{err}: не удалось найти Todo с id {id}")
        raise err

    body = {
        "doc": {
            "completed": completed
        }
    }

    try:
        result = client.update(index=index_name, id=todo_id_in_elastic, body=body)
        return result
    except Exception as err:
        print(f"{err}: не удалось поменять Todo")
        raise err


def delete_todo_from_elastic(id):
    """
    Функция удаления todo из elastic
    :return: 0
    """
    query = {
        "match": {
            "id": id
        }
    }

    try:
        result = client.search(index=index_name, query=query)
        todo_id_in_elastic = result["hits"]["hits"][0]["_id"]
        client.delete(index=index_name, id=todo_id_in_elastic)
        return 0
    except Exception as err:
        print(f"{err}: не удалось удалить Todo с id {id}")
        raise err


def search_dates_in_elastic(dates, flag):
    """
    Функция поиска todo по датам создания
    :return: todos_ids
    """
    if flag == 1:
        query = {
            "bool": {
                "must":
                {
                    "range": {
                        "date_of_create": {
                            "gte": dates[0],
                            "lte": dates[1]
                        }
                    }
                }
            }
        }
    elif flag == 2:
        query = {
            "bool": {
                "must":
                    {
                        "range": {
                            "date_of_create": {
                                "gte": dates[0],
                            }
                        }
                    }
            }
        }
    elif flag == 3:
        query = {
            "bool": {
                "must":
                    {
                        "range": {
                            "date_of_create": {
                                "lte": dates[0],
                            }
                        }
                    }
            }
        }

    todos_ids = []

    try:
        result = client.search(index=index_name, query=query)
        for hit in result["hits"]["hits"]:
            todos_ids.append(hit["_source"]["id"])
        return todos_ids
    except Exception as err:
        print(f"{err}: не удалось найти todo в заданном диапозоне")
        raise err


def add_text_todo_in_elastic(id, text):
    """
    Функция добавления файла todo в elastic
    :return: result
    """
    query = {
        "match": {
            "id": id
        }
    }

    try:
        result = client.search(index=index_name, query=query)
        todo_id_in_elastic = result["hits"]["hits"][0]["_id"]
    except Exception as err:
        print(f"{err}: не удалось найти Todo с id {id}")
        raise err

    body = {
        "doc": {
            "text_file": text
        }
    }

    try:
        result = client.update(index=index_name, id=todo_id_in_elastic, body=body)
        return result
    except Exception as err:
        print(f"{err}: не удалось добавить файл в Todo")
        raise err

