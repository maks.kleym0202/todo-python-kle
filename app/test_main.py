import pytest
import os
import pandas as pd
import json

from pathlib import Path
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from fastapi import Depends
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from database import Base
from main import app, get_db
from models import Todo

SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"

ENGINE = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=ENGINE)

# Для поключения к основной базе данных

# SQLALCHEMY_DATABASE_URL = "postgresql://user:user@postgres:5432/todos"
# ENGINE = create_engine(SQLALCHEMY_DATABASE_URL)
# TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=ENGINE)

Base.metadata.create_all(bind=ENGINE)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


def test_import_todo_from_file():
    file_path = "../data_for_tests/import_for_test.xlsx"
    if os.path.isfile(file_path):
        _files = {'file': open(file_path, 'rb')}
        response = client.post('/upload_files/import/test',
                               files=_files
                               )
        data = response.json()
        assert response.status_code == 200
        assert data["count"] == 11
    else:
        pytest.fail("Scratch file does not exists.")


def test_edit_todo():
    response = client.post('/edit/545/test', data={"description": "new description",
                                                   "completed": "True",
                                                   "tag": "private"})
    data = response.json()

    assert response.status_code == 200
    assert data["description"] == "new description"
    assert data["completed"] is True
    assert data["tag"] == "private"


def test_delete_todo():
    response = client.get("/delete/550/test")
    data = response.json()
    assert data["todo_count"] == 10


def test_list():
    response = client.get('/list/test')
    data = response.json()

    assert response.status_code == 200
    assert len(data["todos"]) == 10


def test_add():
    response = client.post('/add/test', data={"title": "title",
                                              "description": "description",
                                              "belong_to": "general",
                                              "tag": "private"})
    data = response.json()

    assert response.status_code == 200
    assert data["todo"]["title"] == "title"
    assert data["todo"]["description"] == "description"
    assert data["todo"]["belong_to"] == "general"
    assert data["todo"]["tag"] == "private"


def test_import_issues():
    private_token = "glpat-zxYBUYe17bhay2TdyyvQ"
    gitlab_url = "https://gitlab.com/Izoldick/todo-lr3-testing-import"
    response = client.post("/import/git_test",
                           data={
                               'gitlab_url': gitlab_url,
                               'private_token': private_token

                           })
    data = response.json()
    assert response.status_code == 200
    assert data["count"] == 16


def test_import_log():
    file_path = "../data_for_tests/log_for_test.xlsx"
    list_of_files_before = []
    list_of_files_after = []
    directory = 'import_excel'
    pathlist = Path(directory).glob('*.xlsx')
    for path in pathlist:
        list_of_files_before.append(path)

    len_of_list_before = len(list_of_files_before)
    if os.path.isfile(file_path):
        _files = {'file': open(file_path, 'rb')}
        response = client.post('/upload_files/import/test',
                               files=_files
                               )
        data = response.json()
        pathlist2 = Path(directory).glob('*.xlsx')
        for path in pathlist2:
            list_of_files_after.append(path)

        len_of_list_after = len(list_of_files_after)
        assert len_of_list_before + 1 == len_of_list_after
    else:
        pytest.fail("Scratch file does not exists.")
