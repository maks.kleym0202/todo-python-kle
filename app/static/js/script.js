function send_question(text)
{
    if (confirm("Вы уверены что хотите " + text))
    return true;
    else return false;
}


document.addEventListener('DOMContentLoaded', async function(){

    const response = await fetch('/get_cookies', {
        method: 'POST'
    });

    var data = await response.json();

    if (data["token"] === null) {
      document.getElementById("logout-nav").style.display = "none";
    }
});


document.addEventListener('DOMContentLoaded', async function(){

    const response = await fetch('/get_cookies', {
        method: 'POST'
    });

    var data = await response.json();

    if (data["token"] != null) {
      document.getElementById("login-nav").style.display = "none";
    }
});


function submitForm(todo_id)
{
    console.log(todo_id);
    if (document.getElementById(todo_id).value == "Отменить выполнение") {
        document.getElementById(todo_id).value = "Выполнено";
        document.getElementById(todo_id).style.backgroundColor = "#4CAF50";
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open( "GET", "/edit_complete/" + todo_id + "?check=0", true ); // false for synchronous request
        xmlHttp.send( null );
        return xmlHttp.responseText;
    } else {
        document.getElementById(todo_id).value = "Отменить выполнение";
        document.getElementById(todo_id).style.backgroundColor = "red";
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open( "GET", "/edit_complete/" + todo_id + "?check=1", true ); // false for synchronous request
        xmlHttp.send( null );
        return xmlHttp.responseText;
    }
}

