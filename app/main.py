"""Main of todo app
"""
import base64
import os
import math
import pandas as pd
import matplotlib.pyplot as plt
import squarify
import jwt
import gitlab
import random

from pathlib import Path
from fastapi.encoders import jsonable_encoder
from fastapi.openapi.docs import get_swagger_ui_html
from loguru import logger
from datetime import date, datetime, timedelta
from passlib.context import CryptContext
from jwt import PyJWTError
from fastapi import FastAPI, Request, Depends, Form, status, HTTPException, Query, File, UploadFile, Response
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse, JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from starlette.datastructures import URL
from starlette.responses import FileResponse
from starlette.status import HTTP_403_FORBIDDEN
from sqlalchemy.exc import IntegrityError

import models
import crud

from models import Tag, Belong_To, User
from database import init_db, get_db, Session
from team_todos import team_todos
from config import settings
from hashing import Hasher

from elastic import *

init_db()
create()

# pylint: disable=invalid-name
templates = Jinja2Templates(directory="templates")

app = FastAPI()

logger = logger.opt(colors=True)
# pylint: enable=invalid-name

app.mount("/static", StaticFiles(directory="static"), name="static")

fake_users_db = {
    "user": {
        "username": "user",
        "full_name": "user user",
        "email": "user@example.com",
        "hashed_password": "$2b$12$jdwd57cJUF95beV2lCBXueKGi9raDLabkUDFTuvwbFaxgBpovIRmq",
        "disabled": False
    }
}


@app.get("/")
async def home(request: Request,
               delete: int = Query(default=0),
               lo: int = Query(default=0),
               ):
    """Main page with todo list
    """
    logger.info("At home")

    # lo_flag = ""
    msg = ""
    alert = ""

    # token = request.cookies.get("access_token")
    # if token:
    # lo_flag = 1

    if delete == 1:
        os.remove("foo.png")

    if lo == 1:
        msg = "Successfully Logged Out!"
    elif lo == 11:
        alert = "You should be authenticated to Log Out!"

    return templates.TemplateResponse("index.html",
                                      {"request": request,
                                       "team_todos": team_todos,
                                       "team": {1: "2020-3-05-kle",
                                                2: "2020-3-21-tre",
                                                3: "2020-3-03-zhe"},
                                       "msg": msg,
                                       "err": alert,
                                       }
                                      )


@app.get("/registration")
def registration(request: Request):
    return templates.TemplateResponse("registration.html", {"request": request})


@app.post("/registration")
async def registration(request: Request, db: Session = Depends(get_db)):
    form = await request.form()
    username = form.get("username")
    password = form.get("password")

    if not password or len(password) < 6:
        return templates.TemplateResponse(
            "registration.html", {"request": request, "error": "Password should be greater than 6 chars"}
        )
    if not username:
        return templates.TemplateResponse(
            "registration.html", {"request": request, "error": "Username can't be blank"}
        )

    user = User(username=username, password=Hasher.get_hashed_password(password))

    try:
        db.add(user)
        db.commit()
        db.refresh(user)
        return templates.TemplateResponse(
            "registration.html", {"request": request, "msg": "Successfully registered!"}
        )
    except IntegrityError:
        return templates.TemplateResponse(
            "registration.html", {"request": request, "error": "Username already exists"}
        )


@app.get("/login")
def login(request: Request):
    return templates.TemplateResponse("login.html", {"request": request})


@app.post("/login")
async def login(response: Response, request: Request, db: Session = Depends(get_db)):
    form = await request.form()
    username = form.get("username")
    password = form.get("password")

    logger.info(f"Из формы получены username: {username}, password: {password}")

    try:
        user = db.query(User).filter(User.username == username).first()

        if not user:
            return templates.TemplateResponse(
                "login.html", {"request": request, "error": "Invalid username or password"}
            )
        else:
            if Hasher.verify_password(password, user.password):

                data = {"sub": username}
                jwt_token = jwt.encode(
                    data, settings.SECRET_KEY, algorithm=settings.ALGORITHM
                )

                response = RedirectResponse(url="/list",
                                            status_code=status.HTTP_303_SEE_OTHER)
                response.set_cookie(
                    key="access_token", value=f"Bearer {jwt_token}", httponly=True
                )

                return response
            else:
                return templates.TemplateResponse(
                    "login.html", {"request": request, "error": "Invalid username or password"}
                )
    except:
        return templates.TemplateResponse(
            "login.html", {"request": request, "error": "Something Wrong while authentication or storing tokens!"}
        )


@app.get("/logout")
async def logout(response: Response, request: Request):
    lo = 1
    token = request.cookies.get("access_token")
    if not token:
        lo = 11
    response = RedirectResponse(url=f"/?lo={lo}")
    response.delete_cookie("access_token")
    return response


@app.post("/add")
# pylint: disable=too-many-arguments
# pylint: disable=unused-argument
async def todo_add(request: Request, title: str = Form(default=""),
                   description: str = Form(default="", max_length=500),
                   tag: str = Form(default="plans"),
                   belong_to: Belong_To = Form(default="general"),
                   file: UploadFile = File(...),
                   limit: int = Query(),
                   db: Session = Depends(get_db)
                   ):
    """Add new todo
    """

    try:
        token = request.cookies.get("access_token")
        if not token:
            logger.error(f"No token")
            return RedirectResponse(
                url=app.url_path_for(name="list") + "?error=1",
                status_code=status.HTTP_303_SEE_OTHER,
            )

        scheme, _, param = token.partition(" ")
        payload = jwt.decode(param, settings.SECRET_KEY, algorithms=settings.ALGORITHM)
        username = payload.get("sub")
        if username is None:
            logger.error(f"Username is None")
            return RedirectResponse(
                url=app.url_path_for(name="list") + "?error=1",
                status_code=status.HTTP_303_SEE_OTHER,
            )
        else:
            user = db.query(User).filter(User.username == username).first()
            if user is None:
                logger.error(f"User is None")
                return RedirectResponse(url=app.url_path_for("list"), status_code=status.HTTP_303_SEE_OTHER)
            else:
                warning = None
                if description == "":
                    return RedirectResponse(url=app.url_path_for("list") + "?error=3",
                                            status_code=status.HTTP_303_SEE_OTHER)
                else:

                    date_of_create = date.today()

                    logger.info(f"Creating new todo: {file.filename}")
                    if file.filename != "":
                        content = file.file.read()
                        path_to_image = f"todos_images/{file.filename.replace(' ', '_')}"
                        with open(path_to_image, "wb") as file_:
                            file_.write(content)

                        hash_image = Hasher.image_in_hash(path_to_image)

                        if str(hash_image) in crud.dublicate_images_find(db).keys():
                            warning = 1
                        todo = crud.create_todo(db, models.Todo(title=title, description=description,
                                                                tag=tag, date_of_create=date_of_create,
                                                                belong_to=belong_to,
                                                                path_to_image=path_to_image,
                                                                hash_image=hash_image))
                        if belong_to == "general":
                            add_todo_to_elastic(todo.id, title, description, 'false', tag, date_of_create, 'None',
                                                'general',
                                                'None', 'None')
                        else:
                            add_todo_to_elastic(todo.id, title, description, 'false', tag, date_of_create, 'None',
                                                belong_to.value,
                                                'None', 'None')
                    else:
                        todo = crud.create_todo(db, models.Todo(title=title, description=description,
                                                                tag=tag, date_of_create=date_of_create,
                                                                belong_to=belong_to))
                        if belong_to == "general":
                            add_todo_to_elastic(todo.id, title, description, 'false', tag, date_of_create, 'None',
                                                'general', 'None', 'None')
                        else:
                            add_todo_to_elastic(todo.id, title, description, 'false', tag, date_of_create, 'None',
                                                belong_to.value,
                                                'None', 'None')
                    logger.info(f"Creating new todo: {todo}")
                    if warning is not None:
                        return RedirectResponse(url=app.url_path_for("list") + f"?limit={limit}&warning={warning}",
                                                status_code=status.HTTP_303_SEE_OTHER)
                    return RedirectResponse(url=app.url_path_for("list") + f"?limit={limit}",
                                            status_code=status.HTTP_303_SEE_OTHER)
    except Exception as e:
        logger.error(f"Token error: {e}")
        return RedirectResponse(url=app.url_path_for("list"), status_code=status.HTTP_303_SEE_OTHER)


# @app.post("/list")
# async def todo_complete(request: Request):
#     return


@app.post("/add/test")
# pylint: disable=too-many-arguments
# pylint: disable=unused-argument
async def todo_add(request: Request, title: str = Form(default=""),
                   description: str = Form(default="", max_length=500),
                   tag: str = Form(default="plans"),
                   belong_to: Belong_To = Form(default="general"),
                   database: Session = Depends(get_db)):
    """Add new todo
    """
    if description == "":
        logger.error("Empty description")
        raise HTTPException(status_code=400, detail="description must be not empty")

    date_of_create = date.today()
    todo = crud.create_todo(database, models.Todo(title=title, description=description,
                                                  tag=tag, date_of_create=date_of_create,
                                                  belong_to=belong_to))

    logger.info(f"Creating new todo: {todo}")

    todo = {"title": todo.title, "description": todo.description, "belong_to": todo.belong_to,
            "tag": todo.tag}

    return {"todo": todo}


@app.get("/edit/{todo_id}")
async def todo_get(request: Request,
                   todo_id: int,
                   page: int = Query(),
                   database: Session = Depends(get_db)):
    """Get todo
    """
    todo = crud.get_todo_by_id(database, todo_id)
    logger.info(f"Getting todo: {todo}")
    todos = crud.get_all_todo(database)
    loaded_images_temp = []
    for i in todos:
        if i.path_to_image != "":
            loaded_images_temp.append(i.path_to_image[i.path_to_image.find("/") + 1:])
    loaded_images = set(loaded_images_temp)
    return templates.TemplateResponse("edit.html",
                                      {"request": request, "todo": todo, "tags": Tag, "page": page,
                                       "loaded_images": loaded_images})


@app.get("/edit_complete/{todo_id}")
# pylint: disable=unused-argument
async def todo_edit_complete(
        request: Request,
        todo_id: int,
        check: str = Query(),
        database: Session = Depends(get_db)):
    """Edit compete of todo
    """
    todo = crud.get_todo_by_id(database, todo_id)
    if todo is None:
        logger.error("TODO NOT FOUND")
        raise HTTPException(status_code=400, detail=f"TODO NOT FOUND!", )
    else:
        crud.update_todo_complete(database, todo_id, int(check))
        if check == '0':
            edit_complete_todo_in_elastic(todo_id, completed=False)
        else:
            edit_complete_todo_in_elastic(todo_id, completed=True)
        logger.info(f"Success update complete")


@app.post("/edit/{todo_id}")
# pylint: disable=too-many-arguments
# pylint: disable=unused-argument
async def todo_edit(
        request: Request,
        todo_id: int,
        description: str = Form(...),
        completed: bool = Form(False),
        tag: Tag = Form(),
        page: int = Query(),
        choosing_file: str = Form(),
        file: UploadFile = File(...),
        database: Session = Depends(get_db)):
    """Edit todo
    """
    todo = crud.get_todo_by_id(database, todo_id)
    warning = None
    if todo is None:
        logger.error("TODO NOT FOUND")
        raise HTTPException(status_code=400, detail=f"TODO NOT FOUND!", )
    else:
        if choosing_file != "empty":
            path_to_image = f"todos_images/{choosing_file}"
            hash_image = Hasher.image_in_hash(path_to_image)
            if str(hash_image) in crud.dublicate_images_find(database):
                warning = 1
            todo = crud.update_todo(database, todo_id, description, completed, tag,
                                    path_to_image, str(hash_image))
        elif file.filename != "":
            if file.filename.endswith(".txt"):
                content = file.file.read().decode('utf-8')
                print(content)
                add_text_todo_in_elastic(todo_id, content)
                path_to_image = f"todos_text/{file.filename.replace(' ', '_')}"
                with open(path_to_image, "w") as file_:
                    file_.write(content)
            else:
                content = file.file.read()
                path_to_image = f"todos_images/{file.filename.replace(' ', '_')}"
                with open(path_to_image, "wb") as file_:
                    file_.write(content)
                hash_image = Hasher.image_in_hash(path_to_image)
                if str(hash_image) in crud.dublicate_images_find(database):
                    warning = 1
                todo = crud.update_todo(database, todo_id, description, completed, tag,
                                        path_to_image=path_to_image, hash_image=str(hash_image))
        else:
            todo = crud.update_todo(database, todo_id, description, completed, tag)
            edit_todo_in_elastic(todo_id, description, tag.value, completed)
        logger.info(f"Success update {todo}")
    if warning is not None:
        return RedirectResponse(url=app.url_path_for("list") + f"?page={page}&warning=1",
                                status_code=status.HTTP_303_SEE_OTHER)
    return RedirectResponse(url=app.url_path_for("list") + f"?page={page}",
                            status_code=status.HTTP_303_SEE_OTHER)


@app.post("/edit/{todo_id}/test")
async def todo_edit_for_test(
        request: Request,
        todo_id: int,
        description: str = Form(...),
        completed: bool = Form(False),
        tag: Tag = Form(),
        database: Session = Depends(get_db)):
    """Edit todo
    """
    todo = crud.get_todo_by_id(database, todo_id)
    if todo is None:
        logger.error("TODO NOT FOUND")
        raise HTTPException(status_code=400, detail=f"TODO NOT FOUND!", )
    else:
        todo = crud.update_todo(database, todo_id, description, completed, tag)
        logger.info(f"Success update {todo}")

    return {"description": todo.description,
            "completed": todo.completed,
            "tag": todo.tag}


@app.get("/delete/{todo_id}")
# pylint: disable=unused-argument
async def todo_delete(request: Request,
                      todo_id: int,
                      page: int = Query(),
                      database: Session = Depends(get_db)):
    """Delete todo
    """
    todo = crud.get_todo_by_id(database, todo_id)
    if todo is None:
        logger.error("TODO NOT FOUND")
        raise HTTPException(status_code=400, detail=f"TODO NOT FOUND!", )
    else:
        crud.delete_todo(database, todo_id)
        delete_todo_from_elastic(todo_id)
        logger.info(f"Success delete")

    return RedirectResponse(url=app.url_path_for("list") + f"?page={page}",
                            status_code=status.HTTP_303_SEE_OTHER)


@app.get("/delete/{todo_id}/test")
# pylint: disable=unused-argument
async def todo_delete(request: Request,
                      todo_id: int,
                      database: Session = Depends(get_db)):
    """Delete todo
    """
    todo = crud.get_todo_by_id(database, todo_id)
    if todo is None:
        logger.error("TODO NOT FOUND")
        raise HTTPException(status_code=400, detail=f"TODO NOT FOUND!", )
    else:
        crud.delete_todo(database, todo_id)
        logger.info(f"Success delete")

    todo_count = crud.count_todo(database)
    return {"todo_count": todo_count}


@app.get("/list")
async def list(request: Request, page: int = Query(default=0),
               begin_date_filter: str = Query(default=""),
               end_date_filter: str = Query(default=""),
               database: Session = Depends(get_db),
               limit: int = Query(default=10),
               error: int = Query(default=0),
               warning: int = Query(default=None),
               tag: str = Query(default="all")):
    """List page with todo list
    """
    skip = page * limit
    alert = ""
    dict_dublicate_images = crud.dublicate_images_find(database)

    if tag == "plans":
        todo_ids_tag = search_todo_tag("plans")
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids_tag))
    elif tag == "work":
        todo_ids_tag = search_todo_tag("work")
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids_tag))
    elif tag == "private":
        todo_ids_tag = search_todo_tag("private")
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids_tag))
    elif tag == "issues":
        todo_ids_tag = search_todo_tag("issues")
        print(todo_ids_tag)
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids_tag))
    elif tag == "all":
        todos = database.query(models.Todo).order_by(models.Todo.id.desc())
    else:
        raise HTTPException(status_code=400, detail="Unexpected sort parameter was passed")

    if str(begin_date_filter) != "":
        dates = [begin_date_filter]
        todo_ids_dates = search_dates_in_elastic(dates, 2)
        todos = todos.filter(models.Todo.id.in_(todo_ids_dates))
        if str(end_date_filter) != "":
            dates.append(end_date_filter)
            todo_ids_dates = search_dates_in_elastic(dates, 1)
            todos = todos.filter(models.Todo.id.in_(todo_ids_dates))
    if str(end_date_filter) != "":
        dates = [end_date_filter]
        todo_ids_dates = search_dates_in_elastic(dates, 3)
        todos = todos.filter(models.Todo.id.in_(todo_ids_dates))

    count = todos.count()
    todos = todos.offset(skip).limit(limit)
    page_count = math.ceil(count / limit)

    logger.info("Get /list")
    if warning == 1:
        alert = "Loaded image in modified/added todo is dublicate"

    if error == 1:
        alert = "Authenticate by login first"

    if error == 2:
        alert = "Add todo for each tag"
    if error == 3:
        alert = "Заполните поле описания"
    return templates.TemplateResponse("list.html",
                                      {"request": request, "todos": todos,
                                       "page_count": page_count, "skip": skip,
                                       "page": page,
                                       "begin_date_filter": begin_date_filter,
                                       "end_date_filter": end_date_filter,
                                       "limit": limit,
                                       "alert": alert,
                                       "tag": tag,
                                       "dict_dublicate_images": dict_dublicate_images
                                       })


@app.get("/list/test")
async def list(request: Request, database: Session = Depends(get_db)):
    """List page with todo list
    """

    todos = crud.get_all_todo(database)

    logger.info("Get /list/test")
    response = [{
        "title": todo.title,
        "description": todo.description,
        "completed": todo.completed,
        "tag": todo.tag,
        "belong_to": todo.belong_to
    }
        for todo in todos]
    return {"todos": response}


@app.get("/upload_files")
def upload(request: Request):
    """response html page
    """
    return templates.TemplateResponse("import_export.html", {"request": request})


@app.get("/import_log")
def upload(request: Request):
    """response html page
    """
    list_of_files = []
    directory = 'import_excel'
    pathlist = Path(directory).glob('*.xlsx')
    for path in pathlist:
        pathstr = str(path).split("/")[-1]
        list_of_files.append(pathstr)
    return templates.TemplateResponse("import_excel.html", {"request": request, "list_of_files": list_of_files})


@app.post("/upload_files/import")
def upload_import(request: Request,
                  file: UploadFile = File(...),
                  database: Session = Depends(get_db)):
    """ from excel
            """

    token = request.cookies.get("access_token")
    if not token:
        return templates.TemplateResponse("import_export.html",
                                          {"request": request, "error": "Authenticate by login first"})

    scheme, _, param = token.partition(" ")
    payload = jwt.decode(param, settings.SECRET_KEY, algorithms=settings.ALGORITHM)
    username = payload.get("sub")
    if username is None:
        return templates.TemplateResponse("import_export.html",
                                          {"request": request, "error": "Authenticate by login first"})
    else:
        user = database.query(User).filter(User.username == username).first()
        if user is None:
            return templates.TemplateResponse("import_export.html",
                                              {"request": request, "error": "Authenticate by login first"})
        else:
            if file.filename.split('.')[-1] != "xlsx":
                logger.error("File must have .xlsx extension")
                return templates.TemplateResponse("import_export.html",
                                                  {"request": request, "error": "File must have .xlsx extension"})
                # raise HTTPException(status_code=400, detail="File must have .xlsx extension", )
            try:
                logger.info(file.filename)
                logger.info("File reading...")
                content = file.file.read()
                with open("import_excel/" + file.filename, "wb") as file_:
                    file_.write(content)
                data_frame = pd.read_excel(content,
                                           engine="openpyxl",
                                           skiprows=0,
                                           usecols='A:H').values.tolist()
                file.file.close()
                for record in data_frame:
                    todo = models.Todo(id=int(record[0]), title=record[1],
                                       description=record[2], tag=record[4],
                                       date_of_create=crud.to_format_date(record[5]),
                                       date_of_complete=record[6],
                                       belong_to=record[7], completed=record[3])
                    database.add(todo)
                logger.info("Imported todos added to database")
                database.commit()
            except Exception as err_:
                logger.info("Reading error")
                return {"message": f"{err_}"}
            finally:
                file.file.close()
            return templates.TemplateResponse("import_export.html", {"request": request})


@app.post("/upload_files/import/test")
def upload_import(request: Request,
                  file: UploadFile = File(...),
                  database: Session = Depends(get_db)):
    """ from excel
    """
    if file.filename.split('.')[-1] != "xlsx":
        logger.error("File must have .xlsx extension")
        raise HTTPException(status_code=400, detail="File must have .xlsx extension", )
    try:
        logger.info(file.filename)
        logger.info("File reading...")
        content = file.file.read()
        with open("import_excel/" + file.filename, "wb") as file_:
            file_.write(content)
        data_frame = pd.read_excel(content,
                                   engine="openpyxl",
                                   skiprows=0,
                                   usecols='A:H').values.tolist()
        file.file.close()
        for record in data_frame:
            todo = models.Todo(id=int(record[0]), title=record[1],
                               description=record[2], tag=record[4],
                               date_of_create=crud.to_format_date(record[5]),
                               date_of_complete=record[6],
                               belong_to=record[7], completed=record[3])
            database.add(todo)
        logger.info("Imported todos added to database")
        database.commit()
    except Exception as err_:
        logger.info("Reading error")
        return {"message": f"{err_}"}
    finally:
        file.file.close()
    count_todo = crud.count_todo(database)
    response = {"count": count_todo}
    return response


@app.post("/upload_files/export")
# pylint: disable=unused-argument
def upload_export(request: Request, database: Session = Depends(get_db)):
    """to excel
    """
    file_path = 'pandas_to_excel.xlsx'
    if os.path.exists(file_path):
        os.remove(file_path)

    data_frame = pd.DataFrame()
    todos = database.query(models.Todo).order_by(models.Todo.id.desc()).all()

    for todo in todos:
        new_row = pd.Series(
            data={'id': todo.id, 'title': todo.title,
                  'description': todo.description, 'completed': todo.completed,
                  'tag': todo.tag.name, 'date_of_create': todo.date_of_create,
                  'date_of_complete': todo.date_of_complete,
                  'belong_to': todo.belong_to.name}, name=todo.id)

        data_frame = data_frame.append(new_row)

    data_frame.to_excel(file_path, index=False)

    headers = {'Content-Disposition': 'attachment; filename="todos.xlsx"'}
    return FileResponse(file_path, headers=headers)


@app.get("/foo.png")
async def image_resp():
    """Response foo.png
    """
    return FileResponse("foo.png")


@app.get("/todos_images/{file}")
async def todos_image(file: str):
    """Функция отображения картинки в обзем списке тудушек
    """
    return FileResponse(f"todos_images/{file}")


@app.get("/edit/todos_images/{file}")
async def todos_image(file: str):
    """Функция отображения картинки на странице изменения тудушки
    """
    return FileResponse(f"todos_images/{file}")


@app.get("/visualize")
async def todo_visualize(request: Request, database: Session = Depends(get_db)):
    """visualize todo
    """
    count_private_todo = crud.count_todo_by_tag(database, "private")
    count_plans_todo = crud.count_todo_by_tag(database, "plans")
    count_work_todo = crud.count_todo_by_tag(database, "work")

    if count_work_todo == 0 or count_plans_todo == 0 or count_private_todo == 0:
        logger.error("all tags must be not zero")
        return RedirectResponse(
            url=app.url_path_for(name="list") + "?error=2",
            status_code=status.HTTP_303_SEE_OTHER,
        )

    values = [count_private_todo, count_plans_todo, count_work_todo]
    names = [Tag.private.name, Tag.plans.name, Tag.work.name]
    colors = ['#91DCEA', '#64CDCC', '#5FBB68']

    # create a plot figure with
    plt.figure(figsize=(12, 6))
    # pylint: disable=unused-argument
    axis = squarify.plot(values,
                         label=names,
                         color=colors,
                         alpha=0.7,
                         pad=1,
                         text_kwargs={'fontsize': 18})
    # we don't require the axis values so lets remove it
    plt.axis("off")
    plt.savefig('foo.png', bbox_inches='tight')
    return templates.TemplateResponse("visualize.html", {"request": request})


@app.post("/import/git")
async def todo_import_from_git(request: Request,
                               gitlab_url: str = Form(default=""),
                               private_token: str = Form(default=""),
                               database: Session = Depends(get_db),
                               ):
    """
    Import todo from GitLab
    """

    try:
        gl = gitlab.Gitlab(private_token=private_token)
        gl.auth()
    except Exception as err_:
        logger.error(f"Error to connect GitLab: {err_}")
        return templates.TemplateResponse("import_export.html",
                                          {"request": request, "err": "| Error to connect GitLab | "})

    if "https://gitlab.com/" in gitlab_url:
        project_name_with_namespace = gitlab_url.replace("https://gitlab.com/", "")
        project = gl.projects.get(project_name_with_namespace)
        issues = project.issues.list(state='opened')

        for elem in issues:
            todo_title = elem.attributes["title"]
            todo_description = elem.attributes["description"]
            todo_created_at = elem.attributes["created_at"][:10]
            todo = crud.create_todo(database, models.Todo(title=todo_title, description=todo_description,
                                                          tag="issues",
                                                          date_of_create=crud.to_format_date(todo_created_at),
                                                          belong_to="general"))
            logger.info(f"Creating new todo from {gitlab_url}: {todo}")
        return templates.TemplateResponse("import_export.html",
                                          {"request": request, "msg": "Issues imported successfully!"})
    else:
        return templates.TemplateResponse("import_export.html", {"request": request,
                                                                 "err": "| Неверный формат URL | >> Example: https://gitlab.com/artem.tretykov74/test"})


@app.post("/import/git_test")
async def todo_import_from_git(request: Request,
                               gitlab_url: str = Form(default=""),
                               private_token: str = Form(default=""),
                               database: Session = Depends(get_db),
                               ):
    """
    Import todo from GitLab
    """
    try:
        gl = gitlab.Gitlab(private_token=private_token)
    except Exception as err_:
        logger.error(f"Error to connect GitLab: {err_}")
        return {"message": "| Неверный Personal Access Token | "}

    if "https://gitlab.com/" in gitlab_url:
        project_name_with_namespace = gitlab_url.replace("https://gitlab.com/", "")

        project = gl.projects.get(project_name_with_namespace)
        issues = project.issues.list(state='opened')

        for elem in issues:
            todo_title = elem.attributes["title"]
            todo_description = elem.attributes["description"]
            todo_created_at = elem.attributes["created_at"][:10]
            todo = crud.create_todo(database, models.Todo(title=todo_title, description=todo_description,
                                                          tag="issues",
                                                          date_of_create=crud.to_format_date(todo_created_at),
                                                          belong_to="general"))
            logger.info(f"Creating new todo from {gitlab_url}: {todo}")
        count_todo = crud.count_todo(database)
        response = {"count": count_todo}
        return response
    else:
        return {"message": "| Неверный формат URL | >> Example: https://gitlab.com/artem.tretykov74/test"}


@app.get("/favicon.ico")
async def todo_favicon():
    """Отображение фавикона
    """
    return FileResponse(f"static/favicon.ico")


@app.get("/generator")
def upload(request: Request):
    """response html page
    """
    return templates.TemplateResponse("generator.html", {"request": request})


@app.post("/generator")
async def generator(request: Request,
                    todo_gen_count: int = Form(default=""),
                    database: Session = Depends(get_db)):
    """Generate todo
    """
    tag_list = ['work', 'plans', 'private']
    text = 'Дайте мне белые крылья, - я утопаю в омуте, Через тернии, провода, - в небо, только б не мучаться. Тучкой маленькой обернусь и над твоим крохотным домиком Разрыдаюсь косым дождем; знаешь, я так соскучился!'
    list_description = text.split(",")
    date_of_create = date.today()

    for i in range(todo_gen_count):
        random_index = random.randint(0, 2)
        random_index_description = random.randrange(len(list_description))
        random_tag = tag_list[random_index]
        random_description = list_description[random_index_description]
        title = f"Gen {i + 1}"
        todo = crud.create_todo(database, models.Todo(title=title, description=random_description,
                                                      tag=random_tag, date_of_create=date_of_create,
                                                      belong_to="general"))
        add_todo_to_elastic(todo.id, title, random_description, 'false', random_tag, date_of_create, 'None', 'general',
                            'None', 'None')
        logger.info(f"Creating new todo: {todo.title}")

    return templates.TemplateResponse("generator.html",
                                      {"request": request, "msg": f"Успешно добавлено {todo_gen_count} тудушек"})


@app.post("/get_cookies")
def get_cookies(request: Request):
    token = request.cookies.get("access_token")
    return {"token": token}


@app.get("/search")
async def search(request: Request, page: int = Query(default=0),
                 begin_date_filter: str = Query(default=""),
                 end_date_filter: str = Query(default=""),
                 description: str = Query(default="", max_length=500),
                 database: Session = Depends(get_db),
                 limit: int = Query(default=10),
                 error: int = Query(default=0),
                 warning: int = Query(default=None),
                 tag: str = Query(default="all")):
    """List page with todo list
    """
    skip = page * limit
    alert = ""
    dict_dublicate_images = crud.dublicate_images_find(database)

    todo_ids = search_todo(description)

    if tag == "plans":
        todo_ids_tag = search_todo_tag("plans")
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids)).filter(
            models.Todo.id.in_(todo_ids_tag))
    elif tag == "work":
        todo_ids_tag = search_todo_tag("work")
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids)).filter(
            models.Todo.id.in_(todo_ids_tag))
    elif tag == "private":
        todo_ids_tag = search_todo_tag("private")
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids)).filter(
            models.Todo.id.in_(todo_ids_tag))
    elif tag == "issues":
        todo_ids_tag = search_todo_tag("issues")
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids)).filter(
            models.Todo.id.in_(todo_ids_tag))
    elif tag == "all":
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids)).order_by(models.Todo.id.desc())
    else:
        raise HTTPException(status_code=400, detail="Unexpected sort parameter was passed")

    if str(begin_date_filter) != "":
        dates = [begin_date_filter]
        todo_ids_dates = search_dates_in_elastic(dates)
        print(todo_ids_dates)
        todos = todos.filter(models.Todo.id.in_(todo_ids_dates))
        if str(end_date_filter) != "":
            dates.append(end_date_filter)
            todo_ids_dates = search_dates_in_elastic(dates)
            print(todo_ids_dates)
            todos = todos.filter(models.Todo.id.in_(todo_ids_dates))
    count = todos.count()
    todos = todos.offset(skip).limit(limit)
    page_count = math.ceil(count / limit)

    logger.info("Get /search")
    if warning == 1:
        alert = "Loaded image in modified/added todo is dublicate"

    if error == 1:
        alert = "Authenticate by login first"

    if error == 2:
        alert = "Add todo for each tag"
    if error == 3:
        alert = "Заполните поле описания"
    return templates.TemplateResponse("search.html",
                                      {"request": request, "todos": todos,
                                       "page_count": page_count, "skip": skip,
                                       "page": page,
                                       "limit": limit,
                                       "alert": alert,
                                       "tag": tag,
                                       "dict_dublicate_images": dict_dublicate_images,
                                       "description": description
                                       })


@app.post("/search")
async def search(request: Request, page: int = Query(default=0),
                 begin_date_filter: str = Query(default=""),
                 end_date_filter: str = Query(default=""),
                 description: str = Form(default="", max_length=500),
                 database: Session = Depends(get_db),
                 limit: int = Query(default=10),
                 error: int = Query(default=0),
                 warning: int = Query(default=None),
                 tag: str = Query(default="all")):
    """List page with todo list
    """
    skip = page * limit
    alert = ""
    dict_dublicate_images = crud.dublicate_images_find(database)

    todo_ids = search_todo(description)

    if tag == "plans":
        todo_ids_tag = search_todo_tag("plans")
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids)).filter(
            models.Todo.id.in_(todo_ids_tag))
    elif tag == "work":
        todo_ids_tag = search_todo_tag("work")
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids)).filter(
            models.Todo.id.in_(todo_ids_tag))
    elif tag == "private":
        todo_ids_tag = search_todo_tag("private")
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids)).filter(
            models.Todo.id.in_(todo_ids_tag))
    elif tag == "issues":
        todo_ids_tag = search_todo_tag("issues")
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids)).filter(
            models.Todo.id.in_(todo_ids_tag))
    elif tag == "all":
        todos = database.query(models.Todo).filter(models.Todo.id.in_(todo_ids)).order_by(models.Todo.id.desc())
    else:
        raise HTTPException(status_code=400, detail="Unexpected sort parameter was passed")

    if str(begin_date_filter) != "":
        to_date_bdf = crud.to_format_date(begin_date_filter)
        todos = todos.filter(models.Todo.date_of_create >= to_date_bdf)
        if str(end_date_filter) != "":
            to_date_edf = crud.to_format_date(end_date_filter)
            todos = todos.filter(models.Todo.date_of_create <= to_date_edf)

    count = todos.count()
    todos = todos.offset(skip).limit(limit)
    page_count = math.ceil(count / limit)

    logger.info("Get /search")
    if warning == 1:
        alert = "Loaded image in modified/added todo is dublicate"

    if error == 1:
        alert = "Authenticate by login first"

    if error == 2:
        alert = "Add todo for each tag"
    if error == 3:
        alert = "Заполните поле описания"
    return RedirectResponse(url=app.url_path_for("search") + f"?description={description}",
                            status_code=status.HTTP_303_SEE_OTHER)
