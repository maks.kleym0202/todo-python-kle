"""Todo models
"""
from pydantic import BaseModel
from sqlalchemy import Column, Integer, Boolean, Text
from sqlalchemy.ext.declarative import declarative_base
import enum
from sqlalchemy import Enum, Date


Base = declarative_base()


class Tag(enum.Enum):
    private = "private"
    work = "work"
    plans = "plans"
    issues = "issues"


class Belong_To(enum.Enum):
    general = "general"
    first = "2020-3-05-kle"
    second = "2020-3-21-tre"
    third = "2020-3-03-zhe"


class Todo(Base):
    """Todo model
    """
    __tablename__ = 'todos'
    id = Column(Integer, primary_key=True)
    title = Column(Text)
    description = Column(Text)
    completed = Column(Boolean, default=False)
    tag = Column(Enum(Tag), default="plans")
    date_of_create = Column(Date)
    date_of_complete = Column(Text, default="Не выполнено")
    belong_to = Column(Enum(Belong_To), default="general")
    path_to_image = Column(Text, default="")
    hash_image = Column(Text, default="")
    def __repr__(self):
        return f'<Todo {self.id}>'


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(Text, unique=True, nullable=False, index=True)
    password = Column(Text, nullable=False)
    is_active = Column(Boolean, default=True)