import datetime

from loguru import logger
from sqlalchemy.orm import Session
from datetime import date, datetime

from models import Todo


def get_skip_limit_todo(db: Session, skip: int, limit: int = 10):
    return db.query(Todo). \
        order_by(Todo.id.desc()).offset(skip).limit(limit)


def count_todo(db: Session):
    return db.query(Todo).count()


def get_all_todo(db: Session):
    return db.query(Todo)


def get_todo_by_id(db: Session, todo_id: int):
    return db.query(Todo).filter(Todo.id == todo_id).first()


def create_todo(db: Session, todo: Todo):
    _todo = Todo(title=todo.title, description=todo.description, tag=todo.tag,
                 date_of_create=todo.date_of_create, belong_to=todo.belong_to,
                 path_to_image=todo.path_to_image, hash_image=str(todo.hash_image))
    db.add(_todo)
    db.commit()
    return _todo


def delete_todo(db: Session, todo_id: int):
    _todo = db.query(Todo).filter(Todo.id == todo_id).first()
    db.delete(_todo)
    db.commit()


def update_todo(db: Session, todo_id: int, description: str, completed: bool, tag, path_to_image: str = "",
                hash_image=""):
    _todo = db.query(Todo).filter(Todo.id == todo_id).first()
    _todo.description = description
    _todo.completed = completed
    _todo.tag = tag
    if path_to_image != "":
        _todo.path_to_image = path_to_image
    if hash_image != "":
        _todo.hash_image = str(hash_image)
    if completed is True:
        date_of_complete = date.today()
        _todo.date_of_complete = date_of_complete
    else:
        _todo.date_of_complete = "No checked"

    db.commit()
    return _todo


def update_todo_complete(db: Session, todo_id: int, check: int):
    _todo = db.query(Todo).filter(Todo.id == todo_id).first()
    _todo.completed = check

    if check == 1:
        date_of_complete = date.today()
        _todo.date_of_complete = date_of_complete
    else:
        _todo.date_of_complete = "No checked"

    db.commit()


def count_todo_by_tag(db: Session, tag):
    return len(db.query(Todo).filter(Todo.tag == tag).all())


def dublicate_images_find(db: Session):
    todos = get_all_todo(db)
    dict_dublicates = dict()
    for todo in todos:
        if todo.hash_image != "None" and todo.hash_image != "":
            if todo.hash_image in dict_dublicates:
                dict_dublicates[todo.hash_image].append(todo.id)
            else:
                dict_dublicates[todo.hash_image] = [todo.id]
    for key in list(dict_dublicates):
        if len(dict_dublicates[key]) == 1:
            del dict_dublicates[key]

    return dict_dublicates


def to_format_date(date_str):
    date_format = "%Y-%m-%d"
    if type(date_str) is str:
        datetime.strptime(date_str, date_format)
    return date_str
