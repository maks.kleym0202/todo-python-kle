# Запуск с использованием Docker Compose 
 ```sh
docker compose up --build
```

Приложение запускается на 4000-ом порту.

## Выполнение тестов при работающем приложении
Для начала постройте докер образ
```sh
docker build -t lr1-2020-3-05-kle .
```
Теперь запустите тесты
```sh
docker run --rm --network=todo-lr1_default --name testroutes -e SQLALCHEMY_DATABASE_URL="postgresql://user:user@postgres:5432/todos" lr1-2020-3-05-kle pytest test_main.py
```
```sh
docker run --rm --network=<project-dir>_default --name testroutes -e SQLALCHEMY_DATABASE_URL="postgresql://user:user@postgres:5432/todos" lr1-2020-3-05-kle pytest test_main.py
```
 <br/>
 <br/>


# Запуск с использованием Docker 
Построим докер образ: 

Без прокси:
```sh
docker build -t lr1-2020-3-05-kle .
```

С прокси:
```sh
docker build --build-arg PROXY=http://login:pass@192.168.232.1:3128 -t lr1-2020-3-05-kle .
```

## 1) Запускаем в отдельном контейнере базу данных PostgreSQL
```sh
docker run --name tododb --network=host -p 5432:5432 -e POSTGRES_USER=user -e POSTGRES_PASSWORD=user -e POSTGRES_DB=todos -e PGDATA=/var/lib/postgresql/data/pgdata -d -v "$(pwd)/data":/var/lib/postgresql/data postgres:13.3
```

## 2.1) Запуск приложения от пользователя
```sh
docker run --rm --name usercontainer --network=host -e SQLALCHEMY_DATABASE_URL="postgresql://user:user@localhost:5432/todos" lr1-2020-3-05-kle
```

## 2.2) Запуск приложения от разработчика
```sh
docker run --rm --name devcontainer --network=host -e SQLALCHEMY_DATABASE_URL="postgresql://user:user@localhost:5432/todos" -v `pwd`/:/todo_fastapi_lr1/ lr1-2020-3-05-kle
```
### Чтобы сгенерировать n-ное количество тудушек:
```sh
docker run --rm -w /todo_fastapi_lr1/ --network=host -e SQLALCHEMY_DATABASE_URL="postgresql://user:user@localhost:5432/todos" --name startscript lr1-2020-3-05-kle python3 script.py <количество>
```
 <br/>
 <br/>
 
 # Работа в самом приложении


 Для того чтобы добавить тудушку, или импортировать тудушки из файла, необходимо:
 1. Перейти на страницу **Sign Up** и зарегистрироваться (будьте внимательны: пароль должен содержать не менее 6 символов).
 1. Перейти на страницу **Sign In** и авторизоваться.  
  
 **Log Out** - кнопка предназначена, чтобы выйти из учетной записи 
 
 **Excel log** - переход на страницу, содержащую перечень импортированных .xlsx файлов

 **Импорт / Экспорт** -  переход на страницу, содержащую варианты эскпорта и импорта тудушек

 **Визуализация** - переход на страницу, содержащую tree map тэгов тудушки
 
 **Генерация** - переход на страницу генерация тудушек
 
 **Список** - переход на страницу, со списком всех тудушех и формой добавления тудушки 
 
 **Домой** - переход на главную страницу 
