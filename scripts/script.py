import requests
import sys
import random


def script():
    s = requests.Session()
    url = "http://127.0.0.1:80/add"
    tag_list = ['work', 'plans', 'private']
    text = 'Дайте мне белые крылья, - я утопаю в омуте, Через тернии, провода, - в небо, только б не мучаться. Тучкой маленькой обернусь и над твоим крохотным домиком Разрыдаюсь косым дождем; знаешь, я так соскучился!'
    list_description = text.split(",")
    try:
        kolvo = int(sys.argv[1])
    except Exception as e:
        print(e.message)
        return 1

    for i in range(kolvo):
        random_index = random.randint(0, 2)
        random_index_description = random.randrange(len(list_description))
        random_tag = tag_list[random_index]
        random_description = list_description[random_index_description]
        data = {
            'title': f'Task {i + 1}',
            'description': random_description,
            'tag': random_tag,
        }
        response = s.post(url, data=data)


if __name__ == '__main__':
    script()